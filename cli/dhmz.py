#!/usr/bin/python
# -*- coding: utf-8 -*-

# author: Andrej Dundovic
# date: 5. 2011.
# contact: andrej@dundovic.com.hr
# About: This program downloads and display
#        weather report from DHMZ (www.prognoza.hr) 
#
# Copyright 2011 Andrej Dundovic
# Licence:
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Full-text: http://www.gnu.org/licenses/gpl.html
"""

import urllib
import urllib2
""" BeautifulSoup is py module for HTML and XML processing """
from BeautifulSoup import BeautifulSoup
from BeautifulSoup import BeautifulStoneSoup
import re
import sys

class DHMZ:
    """ class for fetching weather report from DHMZ """
    
    options = '' # default options
    xml_data = ''
    service_url = 'http://prognoza.hr/tri/' # 3-day forecast
    place = ''
    places = list()
    
    def get_list( self ):
	""" get list of XML files """
	
	req = urllib2.Request( self.service_url )
	response = urllib2.urlopen( req )
	
	soup = BeautifulSoup( response )
	
	for tplace in soup.findAll('a'):
	      if '.xml' in tplace.string:
		  self.places.append( self.remove_html_tags( tplace ) )
    
    def get_content( self ):
	""" read XML file in path named 'self.place' """
	
	req = urllib2.Request( self.service_url + self.place )
	response = urllib2.urlopen( req )
	
	self.xml_data = response.read()
	self.format_xml()
    
    def remove_html_tags( self, string ):
	""" remove HTML tags """
	
	p = re.compile(r'<.*?>')
	return p.sub('', str( string ).strip() ).strip()
    
    def find( self, query ):
	""" find wanted place in the list """
	
	query = query.lower()
	results = list() # prepare list of results
	displayit = '' # if query is 100% matched but there is more results
	
	for tplace in self.places:
	    if query in tplace.lower():
		results.append( tplace )
		if tplace[0:-4].lower() == query:
		    displayit = tplace
	
	if len( results ) == 0: # Nothing found
	    print u'Nista nije nađeno'
	
	if len( results ) == 1: # One result
	    self.place = results[0]
	    self.get_content()
	else: # Many results
	    print 'Dostupne prognoze za:'
	    for result in results:
		print result[0:-4]
	    if displayit <> '': # One of them 100% matched
		self.place = displayit
		print "-------------------" # Splitter between results and report
		self.get_content()
		displayit = ''
		
    def read_wind( self, code ):
	""" decode wind code (from XML) """
	
	# wind direction
	if 'NE' in code:
	    direction = u'sjeveroistočni, '
	elif 'NW' in code:
	    direction = 'sjeverozapadni, '
	elif 'SW' in code:
	    direction = 'jugozapadni, '
	elif 'SE' in code:
	    direction = u'jugoistočni, '
	elif 'S' in code:
	    direction = u'južni, '
	elif 'N' in code:
	    direction = 'sjeverni, '
	elif 'E' in code:
	    direction = u'istočni, '
	elif 'W' in code:
	    direction = 'zapadni, '
	elif 'C' in code:
	    direction = ''

	# wind speed
	if '0' in code:
	    speed = 'nema vjetra' 
	elif '1' in code:
	    speed = 'slab do umjeren' # [ 2 m/s - 5 m/s ]
	elif '2' in code:
	    speed = 'umjeren do jak' # [ 5 m/s - 9.9 m/s ]
	elif '3' in code:
	    speed = 'jak do olujan' # [ > 9.9 m/s ]
	
	return direction + speed
    
    def format_xml( self ):
	""" pretty CLI output of forecast """
	
	parts_of_day_names = ['prijepodne', 'poslijepodne', u'noć'] # Morning, afternoon, night
	# symbols description from http://prognoza.hr/metsimboli.html
	# (order of strings is important!)
	symbols = [u'vedro, danju sunčano',
		   u'malo oblačno, danju sunčano',
		   u'umjereno oblačno',
		   u'pretežno oblačno',
		   u'oblačno, ali svijetlo',
		   u'oblačno',
		   u'oblačno i tmurno',
		   u'magla, nebo vedro',
		   u'magla, malo do umjereno oblačno',
		   u'maglovito',
		   u'oblačno i maglovito',
		   u'promjenljivo oblačno uz malu količinu kiše',
		   u'promjenljivo oblačno uz umjerenu količinu kiše',
		   u'promjenljivo oblačno uz znatnu količinu kiše',
		   u'promjenljivo oblačno uz moguću grmljavinu',
		   u'promjenljivo oblačno uz malu količinu kiše te moguću grmljavinu',
		   u'promjenljivo oblačno uz umjerenu količinu kiše te moguću grmljavinu',
		   u'promjenljivo oblačno uz znatnu količinu kiše te moguću grmljavinu',
		   u'promjenljivo oblačno uz malu količinu kiše i snijega',
		   u'promjenljivo oblačno uz umjerenu količinu kiše i snijega',
		   u'promjenljivo oblačno uz znatnu količinu kiše i snijega',
		   u'promjenljivo oblačno uz uz malu količinu snijega',
		   u'promjenljivo oblačno uz snijeg',
		   u'promjenljivo oblačno uz znatnu količinu snijega',
		   u'promjenljivo oblačno uz snijeg te moguću grmljavinu',
		   u'oblačno uz malu količinu kiše',
		   u'oblačno uz umjerenu količinu kiše',
		   u'oblačno uz znatnu količinu kiše',
		   u'oblačno uz moguću grmljavinu',
		   u'oblačno uz malu količinu kiše te moguću grmljavinu',
		   u'oblačno uz umjerenu količinu kiše te moguću grmljavinu',
		   u'oblačno uz znatnu količinu kiše te moguću grmljavinu',
		   u'oblačno uz malu količinu kiše i snijega',
		   u'oblačno uz umjerenu količinu kiše i snijega',
		   u'oblačno uz znatnu količinu kiše i snijega',
		   u'oblačno uz malu količinu snijega',
		   u'oblačno uz umjerenu količinu snijega',
		   u'oblačno uz znatnu količinu snijega',
		   u'magla, promjenjivo oblačno, uz kišu',
		   u'magla, promjenjivo oblačno, uz snijeg',
		   u'oblačno i maglovito uz mogući snijeg',
		   u'oblačno i maglovito uz moguću kišu' ]

	dom = BeautifulStoneSoup( self.xml_data )

	# output to CLI
	i = 1
	print 'Datoteka:', self.place
	for day in dom.findAll( 'dan' ):
	      print '=', day['dtj'], day['datum']
	      dd = 0
	      for part_of_day in day.findAll( 'doba_dana' ):
		  print '\t==', parts_of_day_names[dd]
		  dd += 1
		  output = '\t\t* ' + symbols[int( part_of_day.simbol.string )-1] + \
			   '\n\t\t* vjetar: ' + self.read_wind( part_of_day.vjetar.string ) + \
			   '\n\t\t* temp: [ ' + part_of_day.tmin.string + ' - ' + \
			   str( part_of_day.tmax.string ) + u' ] °C'
		  print output
	      if str(i) in self.options:
		  break
	      i += 1

    def __init__( self, options ):
	""" initialization of class: get list of places """
	
	self.options = options
	self.get_list();

if __name__ == "__main__":
    """ run class and set query """ 
    
    options = '' # N (number) - only first N day(s) of forecast, N_max = 3
    if len(sys.argv) < 2:
	  query = 'xml'
    else:
	  query = sys.argv[1]
	  if len(sys.argv) == 3:
	      options = sys.argv[2]
    
    prognoza = DHMZ( options )
    prognoza.find( query )
